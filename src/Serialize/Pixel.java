package Serialize;

public class Pixel implements MySerialisable {

  public static final Creator<Pixel> CREATOR = Pixel::new;

  private int x;
  private int y;
  private int color;

  public Pixel(int x, int y, int color) {

    this.x = x;
    this.y = y;
    this.color = color;
  }

  public Pixel() {
  }

  public int getX() {
    return x;
  }

  public void setX(int x) {
    this.x = x;
  }

  public int getY() {
    return y;
  }

  public void setY(int y) {
    this.y = y;
  }

  public int getColor() {
    return color;
  }

  public void setColor(int color) {
    this.color = color;
  }

  @Override
  public void writeToBuff(SerializerBuffer sb) {
    sb.writeInt(x);
    sb.writeInt(y);
    sb.writeInt(color);
  }

  @Override
  public void readFromBuff(SerializerBuffer sb) {
    x = sb.readInt();
    y = sb.readInt();
    color = sb.readInt();
  }

  @Override
  public String toString() {
    return " { (" + x + "," + y + ") : " + color + "}\n";
  }

}
