package connection;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.InetSocketAddress;
import java.nio.ByteBuffer;
import java.nio.channels.SelectionKey;
import java.nio.channels.Selector;
import java.nio.channels.ServerSocketChannel;
import java.nio.channels.SocketChannel;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.Set;

import Serialize.Point;
import Serialize.Request;
import Serialize.SerializerBuffer;

public class MyHttpServer implements Runnable {

  static final String HTML_START = "<html>" + "<title>HTTP Server in java</title>" + "<body>";
  static final String HTML_END = "</body>" + "</html>";
  private int H;
  private int W;
  private boolean stop;
  private SerializerBuffer serializer;
  private IOManager manager;
  private ServerSocketChannel socketHttp;
  Selector selectorHttp;
  private ArrayList<SelectionKey> waiting;
  private Server principalServer;

  public MyHttpServer(int port, Server server) {
    stop = false;
    serializer = new SerializerBuffer(1024);
    manager = IOManager.getInstance();
    waiting = new ArrayList<>();
    this.principalServer = server;
    W = 900;
    H = 600;
    try {

      socketHttp = ServerSocketChannel.open();
      socketHttp.bind(new InetSocketAddress(port));
      socketHttp.configureBlocking(false);
      selectorHttp = Selector.open();
      socketHttp.register(this.selectorHttp, SelectionKey.OP_ACCEPT);

    } catch (IOException e) {
      System.err.println("Impossible de demarrer le serveur Http");
      e.printStackTrace();
    }
  }

  @Override
  public void run() {
    Set<SelectionKey> keys = null;
    while (!stop) {
      try {
        selectorHttp.select();
        keys = selectorHttp.selectedKeys();
        keys.forEach(s -> {
          if (s.isValid()) {
            if (s.isAcceptable()) {
              this.accept();
            } else if (s.isReadable()) {
              reciveHttpRequest(s);
            }
          }
        });
        for (int i = 0; i < waiting.size(); i++) {
          SelectionKey s = waiting.get(i);
          String fileName = manager.createFileIfReady(System.identityHashCode(s));
          if (!"".equals(fileName)) {
            System.out.println("Http - file " + fileName + " is ready");
            Request r = manager.historic.get(System.identityHashCode(s));
            updateWebPage(fileName + ".png?Xmin=" + r.getpMin().getRe() + "&Ymin=" + r.getpMin().getIm()
                + "&Xmax=" + r.getpMax().getRe() + "&Ymax=" + r.getpMax().getIm() + "&Iteration="
                + r.getIteration() + "", r.getWidth(), r.getLength());
            try {
              sendResponse(s, 200, "ProjetPW.html", true);
            } catch (Exception e) {
              System.out.println("Envoie de page html annulé");
              s.cancel();
            }
          }
        }
      } catch (IOException e) {

      }
      keys.clear();
    }

  }

  public void accept() {

    try {
      SocketChannel sc = socketHttp.accept();
      if (sc != null) {
        System.out.println("New client " + sc);
        sc.configureBlocking(false);
        sc.register(this.selectorHttp, SelectionKey.OP_READ);
      }
    } catch (IOException e) {
      System.err.println("Impossible d'accepter le client Http");
      e.printStackTrace();
    }
  }

  public boolean reciveHttpRequest(SelectionKey s) {
    Charset charset = Charset.forName("ISO-8859-1");
    String str = "";
    SocketChannel sc = (SocketChannel) s.channel();
    serializer.byteBuffer.clear();
    int nbRead = 0;
    try {
      serializer.byteBuffer.limit(serializer.byteBuffer.capacity());
      nbRead = sc.read(serializer.byteBuffer);

      if (nbRead == -1) {
        s.channel().close();
        s.cancel();
      }
      while (nbRead > 0) {
        serializer.byteBuffer.flip();
        str = str.concat(charset.decode(serializer.byteBuffer).toString());
        serializer.byteBuffer.limit(serializer.byteBuffer.capacity());
        nbRead = sc.read(serializer.byteBuffer);

      }

    } catch (IOException e) {
      System.out.println("probleme de lecture sur socket client http");
    }

    if ("".equals(str))
      return false;
    String[] values = parse(str);
    if (values != null) {
      if (values.length == 1) {
        try {
          sendResponse(s, 200, values[0], true);
          return true;
        } catch (Exception e1) {
          return false;
        }
      } else if (values.length == 6) {
        try {
          double imMin = Double.parseDouble(values[0]);
          double reMin = Double.parseDouble(values[1]);
          double imMax = Double.parseDouble(values[2]);
          double reMax = Double.parseDouble(values[3]);

          System.out.println(imMin + "/" + reMin + "/" + imMax + "/" + reMax);
          System.out.println(Math.abs(imMax - imMin) / Math.abs(reMax - reMin));
          System.out.println((double) W / (double) H);
          int w = W;
          int h = H;
          double r1 = Math.abs(imMax - imMin) / Math.abs(reMax - reMin);
          double r2 = (double) w / (double) h;
          if (r1 < 1) {
            // h = (int)(h / r1);
            w = (int) (r1 * h);
          } else if (r1 < r2) {
            w = (int) (w / r1);
          } else {
            h = (int) (w / r1);
            w = (int) (h * r1);
          }
          System.out.println(W + "//" + h);
          Point pMin = new Point(imMin, reMin);
          Point pMax = new Point(imMax, reMax);
          int iteration = Integer.parseInt(values[4]);

          Request req = new Request(System.identityHashCode(s), pMin, pMax, iteration, w, h, 0, 0);
          System.out.println("Http-Reicived : " + req);
          manager.recive(req);
          principalServer.selector.wakeup();
          waiting.add(s);
          return true;
        } catch (NumberFormatException e) {
          System.out.println("format de donnees non valide");
          return false;
        }
      }
    }
    return false;
  }

  private static String[] parse(String string) {
    String[] keys = new String[6];
    String[] values = null;
    String[] elements = string.split(" ");
    if (!elements[0].equalsIgnoreCase("GET"))
      return null;

    if (elements[1].equalsIgnoreCase("/ProjetPW.html") || elements[1].equalsIgnoreCase("/")) {
      // requete intitail
      // vaut meiux remplacer ça par l envoie de la page initiale
      // directement ( evite le calcule au debut )
      values = new String[6];
      values[0] = "-2";
      values[1] = "-1";
      values[2] = "1";
      values[3] = "1";
      values[4] = "50";
      return values;
    }
    if (elements[1].startsWith("/ProjetPW.html?")) {
      values = new String[6];
      String urlData = elements[1].substring("/ProjetPW.html?".length());
      String[] attributes = urlData.split("&");
      if (attributes.length != 6)
        return null;
      for (int i = 0; i < 6; i++) {
        String[] tmp = attributes[i].split("=");
        if (tmp.length != 2)
          return null;
        keys[i] = tmp[0];
        values[i] = tmp[1];
      }
      if (!keys[0].equals("Xmin") || !keys[1].equals("Ymin") || !keys[2].equals("Xmax") || !keys[3].equals("Ymax")
          || !keys[4].equals("Iteration") || !keys[5].equals("submit"))
        return null;
    } else if (elements[1].startsWith("/localhost") || elements[1].startsWith("/127.0.0.1")) {
      values = new String[1];
      String[] data = elements[1].substring(1).split("/");
      int limit = data[1].indexOf('?');
      values[0] = data[1].substring(0, limit);
    } else if (elements[1].equalsIgnoreCase("/favicon.ico")) {
      values = new String[1];
      values[0] = elements[1].substring(1);
    }
    return values;
  }

  public void sendResponse(SelectionKey s, int statusCode, String responseString, boolean isFile) throws Exception {
    Charset charset = Charset.forName("ISO-8859-1");
    SocketChannel sc = (SocketChannel) s.channel();
    String statusLine = "";
    String serverdetails = "Server: Java HTTPServer";
    String contentLengthLine = "";
    String fileName = "";
    String contentTypeLine = "Content-Type: text/html" + "\r\n";
    FileInputStream fin = null;

    if (statusCode == 200)
      statusLine = "HTTP/1.1 200 OK" + "\r\n";
    else
      statusLine = "HTTP/1.1 404 Not Found" + "\r\n";

    if (isFile) {
      fileName = responseString;
      fin = new FileInputStream(fileName);
      contentLengthLine = "Content-Length: " + Integer.toString(fin.available()) + "\r\n";
      if (!fileName.endsWith(".htm") && !fileName.endsWith(".html"))
        contentTypeLine = "Content-Type: \r\n";
    } else {
      responseString = HTML_START + responseString + HTML_END;
      contentLengthLine = "Content-Length: " + responseString.length() + "\r\n";
    }
    sc.write(charset.encode(statusLine));
    sc.write(charset.encode(serverdetails));
    sc.write(charset.encode(contentTypeLine));
    sc.write(charset.encode(contentLengthLine));
    // sc.write(charset.encode("Connection: Keep-Alive\r\n"));
    // sc.write(charset.encode("Keep-Alive: timeout=10 max=3\r\n"));
    sc.write(charset.encode("\r\n"));

    if (isFile) {
      sendFile(fin, sc);
      waiting.remove(s);
      new File(fileName).delete();
    } else
      sc.write(charset.encode(responseString));
  }

  public void sendFile(FileInputStream fin, SocketChannel out) throws Exception {
    byte[] buffer = new byte[1024];
    while ((fin.read(buffer)) != -1) {
      out.write(ByteBuffer.wrap(buffer));
    }
    fin.close();
  }

  public void updateWebPage(String imageURL, int w, int h) {

    String oldPage = "";
    File fi = new File("init.html");
    DataInputStream is = null;
    DataOutputStream os = null;
    try {
      is = new DataInputStream(new FileInputStream(fi));
      byte[] b = new byte[1024];
      int nb = 0;
      while ((nb = is.read(b)) != -1) {
        oldPage += new String(b, 0, nb);
      }

      String newPage = oldPage.replaceAll("XXXXXX", imageURL);
      newPage = newPage.replaceAll("YYYY", "" + w);
      newPage = newPage.replaceAll("ZZZZ", "" + h);
      File fo = new File("ProjetPW.html");

      os = new DataOutputStream(new FileOutputStream(fo));
      os.write(newPage.getBytes());

    } catch (IOException e) {
      e.printStackTrace();
    } finally {
      try {

        os.close();
        is.close();
      } catch (IOException e) {

      }
    }
  }

  // public static void main(String[] args) {
  // Thread t = new Thread(new MyHttpServer(5000));
  // t.start();
  // }

}