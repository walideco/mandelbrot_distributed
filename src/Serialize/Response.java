package Serialize;

import java.util.ArrayList;

public class Response implements MySerialisable {

  public static final Creator<Response> CREATOR = Response::new;

  private int idRequest;
  private Pixel[] values;

  public Response(int id, Pixel[] values) {
    this.values = values;
    this.idRequest = id;
  }

  public Response() {
  }

  public int getIdRequest() {
    return idRequest;
  }

  public void setIdRequest(int idRequest) {
    this.idRequest = idRequest;
  }

  public void setValues(Pixel[] values) {
    this.values = values;
  }

  public Pixel[] getValues() {
    return values;
  }

  public ArrayList<Response> split() {
    ArrayList<Response> result = new ArrayList<>();
    int index = 0;
    if (this.values.length > 50) {
      Pixel[] tmp = new Pixel[50];
      for (int i = 0; i < values.length; i++) {
        if (index == 50) {
          result.add(new Response(this.idRequest, tmp));
          index = 0;
          if (values.length - i >= 50)
            tmp = new Pixel[50];
          else
            tmp = new Pixel[values.length - i];
        }
        tmp[index] = values[i];
        index++;
      }
      if (tmp.length <= 50)
        result.add(new Response(this.idRequest, tmp));
    } else {
      result.add(this);
    }
    return result;
  }

  @Override
  public void writeToBuff(SerializerBuffer sb) {
    sb.writeInt(idRequest);
    sb.writeInt(values.length);
    for (Pixel c : values) {
      sb.writeMySerializable(c);
    }
  }

  @Override
  public void readFromBuff(SerializerBuffer sb) {
    idRequest = sb.readInt();
    int quantity = sb.readInt();
    values = new Pixel[quantity];
    for (int i = 0; i < quantity; i++) {
      values[i] = sb.readMySerializable(Pixel.CREATOR);
    }
  }

  @Override
  public String toString() {
    String stringValues = "Request ID: " + idRequest + " Pixels : {";
    for (int i = 0; i < this.values.length - 1; i++) {
      stringValues += "    " + values[i] + ";";
    }
    if (values.length > 0)
      stringValues += values[values.length - 1];
    return stringValues + "}";
  }
}
