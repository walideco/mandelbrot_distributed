package Serialize;

import Serialize.Creator;

public class Request implements MySerialisable {

  public static final Creator<Request> CREATOR = Request::new;

  private int id;
  private Point pMin;
  private Point pMax;
  private int iteration;
  private int width;
  private int length;
  private int tX;
  private int tY;

  public Request(int id, Point pMin, Point pMax, int iteration, int width, int length, int tX, int tY) {
    this.id = id;
    this.pMin = pMin;
    this.pMax = pMax;
    this.iteration = iteration;
    this.width = width;
    this.length = length;
    this.tX = tX;
    this.tY = tY;
  }

  public int getId() {
    return id;
  }

  public void setId(int id) {
    this.id = id;
  }

  public int gettX() {
    return tX;
  }

  public void settX(int tX) {
    this.tX = tX;
  }

  public int gettY() {
    return tY;
  }

  public void settY(int tY) {
    this.tY = tY;
  }

  public Request() {
  }

  public Point getpMin() {
    return pMin;
  }

  public void setpMin(Point pMin) {
    this.pMin = pMin;
  }

  public Point getpMax() {
    return pMax;
  }

  public void setpMax(Point pMax) {
    this.pMax = pMax;
  }

  public int getIteration() {
    return iteration;
  }

  public void setIteration(int iteration) {
    this.iteration = iteration;
  }

  public int getWidth() {
    return width;
  }

  public void setWidth(int width) {
    this.width = width;
  }

  public int getLength() {
    return length;
  }

  public void setLength(int length) {
    this.length = length;
  }

  @Override
  public void writeToBuff(SerializerBuffer sb) {
    sb.writeInt(id);
    sb.writeMySerializable(pMin);
    sb.writeMySerializable(pMax);
    sb.writeInt(iteration);
    sb.writeInt(width);
    sb.writeInt(length);
    sb.writeInt(tX);
    sb.writeInt(tY);
  }

  @Override
  public void readFromBuff(SerializerBuffer sb) {
    id = sb.readInt();
    pMin = sb.readMySerializable(Point.CREATOR);
    pMax = sb.readMySerializable(Point.CREATOR);
    iteration = sb.readInt();
    width = sb.readInt();
    length = sb.readInt();
    tX = sb.readInt();
    tY = sb.readInt();
  }

  @Override
  public String toString() {
    return "Request {id= " + id + " pMin= " + pMin + ", pMax= " + pMax + ", iteration= " + iteration + ", width= "
        + width + ", length= " + length + " translation :(" + tX + "," + tY + ") }";
  }

  @Override
  public boolean equals(Object obj) {
    if (this == obj)
      return true;
    if (obj instanceof Request) {
      Request tmp = (Request) obj;
      return tmp.id == id;
    }
    return false;

  }

}
