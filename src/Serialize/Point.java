package Serialize;

public class Point implements MySerialisable {

  public static final Creator<Point> CREATOR = Point::new;

  private double im;
  private double re;

  public Point(double im, double re) {
    this.im = im;
    this.re = re;
  }

  public Point() {
  }

  public double getIm() {
    return im;
  }

  public void setIm(double im) {
    this.im = im;
  }

  public double getRe() {
    return re;
  }

  public void setRe(double re) {
    this.re = re;
  }

  @Override
  public void writeToBuff(SerializerBuffer sb) {
    sb.writeDouble(im);
    sb.writeDouble(re);
  }

  @Override
  public void readFromBuff(SerializerBuffer sb) {
    im = sb.readDouble();
    re = sb.readDouble();
  }

  @Override
  public String toString() {
    return "( " + re + "+" + im + " i )";
  }
}
