package Serialize;

public interface MySerialisable {

  public void writeToBuff(SerializerBuffer sb);

  public void readFromBuff(SerializerBuffer sb);

}