package Serialize;

import java.awt.Color;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class Mandelbrot {

  public static Pixel[] compute(Request request, int c) throws IOException {
    Pixel[] imageFinal = new Pixel[request.getLength() * request.getWidth()];
    int compteur = 0;
    int color;
    for (int x = 0; x < request.getWidth(); x++) {
      for (int y = 0; y < request.getLength(); y++) {
        double cx = Math.abs(request.getpMax().getIm() - request.getpMin().getIm()) * x / request.getWidth()
            + request.getpMin().getIm();
        double cy = Math.abs(request.getpMax().getRe() - request.getpMin().getRe()) * y / request.getLength()
            + request.getpMin().getRe();
        int count = calcMandelbrot(cx, cy, request.getIteration());
        if (count != -1) {
          color = Color.HSBtoRGB(count / 200.0f, 1.0f, 1.0f);
          imageFinal[compteur] = new Pixel(x + request.gettX(),
                            /* request.getLength() - 1 - */ y, color);
        } else {
          imageFinal[compteur] = new Pixel(x + request.gettX(),
							/* request.getLength() - 1 - */ y, 0);
        }
        compteur++;
      }
    }
    return imageFinal;
  }

  public static int calcMandelbrot(double cx, double cy, int maxCount) {
    double zx = 0;
    double zy = 0;
    for (int i = 0; i < maxCount; i++) {
      double sx = zx * zx;
      double sy = zy * zy;
      if ((sx + sy) >= 4.0)
        return i;
      zy = 2.0 * zx * zy + cy;
      zx = sx - sy + cx;
    }
    return -1;
  }

  public static List<Request> subRequestBis(Request request, int nb) {
    List<Request> list = new ArrayList<>();
    int nbCols = nb;
    int nbRows = 1;
    if (nb < 4) {
      nbCols = nb;
      nbRows = 1;
    } else if (nb >= 4) {
      nbCols = (int) Math.sqrt(nb);
      while (nb % nbCols != 0 && nbCols < nb)
        nbCols++;
      if (nbCols == nb)
        nbCols = nb;
      else
        nbRows = nb / nbCols;
    }
    int tX = request.getWidth() / nbCols;
    int tY = request.getLength() / nbRows;
    double tcx = Math.abs(request.getpMax().getIm() - request.getpMin().getIm()) / nbCols;
    double tcy = Math.abs(request.getpMax().getRe() - request.getpMin().getRe()) / nbRows;
    int w = request.getWidth() / nbCols;
    int h = request.getLength() / nbRows;
    for (int i = 0; i < nbCols; i++) {
      for (int j = 0; j < nbRows; j++) {
        if (i == nbCols - 1)
          w = w + request.getWidth() % tX;
        if (j == nbRows - 1)
          h = h + request.getLength() % tY;
        list.add(new Request(request.getId(),
            new Point(request.getpMin().getIm() + tcx * i, request.getpMin().getRe() + tcy * j),
            new Point(request.getpMin().getIm() + tcx * (i + 1), request.getpMin().getRe() - tcy * (j - 1)),
            request.getIteration(), w, h, request.gettX() + tX * i, request.gettY() + tY * j));
      }
    }
    return list;
  }

  public static List<Request> subRequest(Request request, int nbClient) {

    List<Request> ls = new ArrayList<>();
    int tX = request.getWidth() / nbClient;
    int tY = request.getLength() / nbClient;
    double pasX = Math.abs(request.getpMax().getIm() - request.getpMin().getIm()) / nbClient;
    double pasY = Math.abs(request.getpMax().getRe() - request.getpMin().getRe()) / nbClient;
    int w = request.getWidth() / nbClient;
    int l = request.getLength() / nbClient;
    for (int i = 0; i < nbClient; i++) {
      tY = 0;
      if ((i == nbClient - 1) && tX > 0)
        w = w + request.getWidth() % tX;
      ls.add(new Request(request.getId(),
          new Point(request.getpMin().getIm() + pasX * (i), request.getpMin().getRe()),
          new Point(request.getpMin().getIm() + pasX * (i + 1), request.getpMax().getRe()),
          request.getIteration(), w, request.getLength(), request.gettX() + tX * i, tY));
    }
    return ls;
  }

  public static Pixel[] recompose(Pixel[][] ensemble) {
    Pixel[] resultat = ensemble[0];
    int compteur = 0;
    for (int i = 1; i < ensemble.length; i++) {
      for (int j = 1; j < ensemble[i].length; j++)
        resultat[compteur] = ensemble[i][j];
      compteur++;
    }

    return resultat;
  }

  public static double[] splitRequest(String s) {
    double[] res = new double[5];
    int i = 0;
    s = s.substring(14, s.length() - 15);
    String[] t = s.split("&");
    for (String x : t) {

      res[i] = Double.parseDouble(x.split("=")[1]);
      i++;
    }
    System.out.println(res[0] + " " + res[1] + " " + res[2] + " " + res[3] + " " + res[4]);
    return res;

  }
}