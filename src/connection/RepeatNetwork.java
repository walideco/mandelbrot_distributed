package connection;

import static java.util.concurrent.Executors.newFixedThreadPool;

import java.io.IOException;
import java.nio.BufferUnderflowException;
import java.nio.ByteBuffer;
import java.nio.channels.SocketChannel;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Future;

import Serialize.Mandelbrot;
import Serialize.Request;
import Serialize.Response;
import Serialize.SerializerBuffer;
import Serialize.Task;

public class RepeatNetwork implements Runnable {

  private SocketChannel socketChannel;
  private Client client;
  private ExecutorService executor;

  public RepeatNetwork(Client client) {
    this.client = client;
    this.socketChannel = this.client.getSocketChannel();
    this.executor = newFixedThreadPool(Runtime.getRuntime().availableProcessors() / 2);
  }

  @Override
  public void run() {
    System.out.println("Attente de requete de calcul ...");
    SerializerBuffer sb = new SerializerBuffer(1024);
    while (client.isConnected()) {
      Request request = null;
      try {
        sb.byteBuffer.flip();
        int n = 0;
        sb.byteBuffer.limit(sb.byteBuffer.capacity());
        if ((n = socketChannel.read(sb.byteBuffer)) == -1)
          throw new IOException("Connction close");
        if (n > 0) {
          try {
            sb.byteBuffer.flip();
            request = sb.readMySerializable(Request.CREATOR);
          } catch (BufferUnderflowException e) {
            sb.extendSize();
          }
        }
        if (request != null) {

          System.out.println("Requete reçue:\n " + request);
          System.out.println("calcule en cours ....");
          List<Response> responses = new ArrayList<>();
          List<Future<Response>> futures = new ArrayList<>();
          List<Request> subRequest = Mandelbrot.subRequest(request, 8);

          for (Request req : subRequest)
            futures.add(executor.submit(new Task(req)));
          for (Future<Response> future : futures)
            responses.addAll(future.get().split());

          ByteBuffer bb = ByteBuffer.allocate(4);
          System.out.println(responses.size() + " reponse à envoyer");
          bb.putInt(responses.size());
          bb.flip();
          socketChannel.write(bb);

          for (int i = 0; i < responses.size(); i++) {
            sb.byteBuffer.flip();
            sb.byteBuffer.limit(sb.byteBuffer.capacity());
            sb.writeMySerializable(responses.get(i));
            sb.byteBuffer.flip();
            socketChannel.write(sb.byteBuffer);
          }
        }
      } catch (IOException e) {
        System.out.println("déconnexion!");
        client.disconnected();
        executor.shutdown();
      } catch (InterruptedException | ExecutionException e) {
        client.disconnected();
        executor.shutdown();
        e.printStackTrace();
      }
    }

  }

}
