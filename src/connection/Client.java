package connection;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.nio.channels.SocketChannel;

public class Client {
  private SocketChannel socketChannel;
  private boolean isConnected;
  private String name;

  public Client(String name, String address, int port) {
    this.name = name;
    try {
      InetSocketAddress is = new InetSocketAddress(address, port);
      this.socketChannel = SocketChannel.open();
      this.socketChannel.connect(is);
      this.setConnected(true);
    } catch (IllegalArgumentException | IOException e) {
      System.err.println("Connection impossible");
      e.printStackTrace();
    }
  }

  public boolean isConnected() {
    return isConnected;
  }

  public void setConnected(boolean isConnected) {
    this.isConnected = isConnected;
  }

  public SocketChannel getSocketChannel() {
    return socketChannel;
  }

  public String getName() {
    return name;
  }

  public void disconnected() {
    this.setConnected(false);
  }

  public void start() {
    System.out.println("Connection ...");
    Thread threadNetwork = new Thread(new RepeatNetwork(this));
    threadNetwork.start();
  }

  public static void main(String[] args) {
    new Client("test", "localhost", 12345).start();
  }

}
