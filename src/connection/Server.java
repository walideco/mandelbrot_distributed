package connection;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.nio.BufferUnderflowException;
import java.nio.ByteBuffer;
import java.nio.channels.SelectionKey;
import java.nio.channels.Selector;
import java.nio.channels.ServerSocketChannel;
import java.nio.channels.SocketChannel;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import Serialize.Request;
import Serialize.Response;
import Serialize.SerializerBuffer;

public class Server implements Runnable {

  private ServerSocketChannel socket;
  Selector selector;
  private boolean stop;
  private SerializerBuffer serializer;
  private IOManager manager;
  private ArrayList<SocketChannel> freeClient;
  private MyHttpServer httpServer;

  public Server(int port) {
    stop = false;
    serializer = new SerializerBuffer(1024);
    manager = IOManager.getInstance();
    freeClient = new ArrayList<>();
    this.httpServer = null;
    try {

      socket = ServerSocketChannel.open();
      socket.bind(new InetSocketAddress(port));
      socket.configureBlocking(false);
      selector = Selector.open();
      socket.register(this.selector, SelectionKey.OP_ACCEPT);

    } catch (IOException e) {
      System.err.println("Impossible de demarrer le serveur");
      e.printStackTrace();
    }
  }

  public void accept() {
    SocketChannel sc = null;
    try {
      sc = socket.accept();
      if (sc != null) {
        System.out.println("New client " + sc);
        sc.configureBlocking(false);
        sc.register(this.selector, (SelectionKey.OP_READ));
        freeClient.add(sc);
      }
    } catch (IOException e) {
      System.err.println("Impossible d'accepter le client");
      e.printStackTrace();
    }
  }

  public List<Response> reciveResponse(SelectionKey s) {

    List<Response> responses = new ArrayList<>();
    SocketChannel sc = (SocketChannel) s.channel();
    int nbRead = 0, nbReponses = 0;

    ByteBuffer bb = ByteBuffer.allocate(4);
    try {
      nbRead = sc.read(bb);
      if (nbRead == -1) {
        s.channel().close();
        s.cancel();
        freeClient.remove(s);
      } else if (nbRead > 0) {
        bb.rewind();
        nbReponses = bb.getInt();
      }
      serializer.byteBuffer.clear();
      serializer.byteBuffer.limit(serializer.byteBuffer.capacity());
      nbRead = sc.read(serializer.byteBuffer);

      while (nbReponses > 0) {
        if (nbRead == -1) {
          s.channel().close();
          s.cancel();
          freeClient.remove(s);
          System.out.println("Connction client (" + s + ") close");
          return null;
        } else {
          boolean flag = true, flag2 = false;
          serializer.byteBuffer.flip();
          int pos = 0;

          while (flag) {
            try {
              pos = serializer.byteBuffer.position();
              Response r = serializer.readMySerializable(Response.CREATOR);
              if (r.getValues().length > 0) {
                flag2 = true;
                responses.add(r);
                nbReponses--;
              }
            } catch (BufferUnderflowException e) {
              flag = false;
            }
          }
          // if (responses.size() == 0) {
          // serializer.extendSize(); /* attention a verifier */
          // }
          if (flag2) {
            serializer.byteBuffer.position(pos);
            serializer.byteBuffer.compact();
          }
        }
        serializer.byteBuffer.limit(serializer.byteBuffer.capacity());
        nbRead = sc.read(serializer.byteBuffer);
      }
    } catch (IOException e) {
      freeClient.remove(s);
      System.out.println("Probleme de connection");
      return new ArrayList<Response>();
    }
    return responses;
  }

  public void sendRequest(SocketChannel client, Request request) {
    this.serializer.byteBuffer.flip();
    this.serializer.byteBuffer.limit(this.serializer.byteBuffer.capacity());
    this.serializer.writeMySerializable(request);
    serializer.byteBuffer.flip();
    try {

      client.write(serializer.byteBuffer);

    } catch (IOException | BufferUnderflowException e) {

      e.printStackTrace();
    }
  }

  @Override
  public void run() {
    Set<SelectionKey> keys = null;
    while (!stop) {
      try {
        selector.select();
        keys = selector.selectedKeys();
        keys.forEach(s -> {
          if (s.isValid()) {
            if (s.isAcceptable()) {
              accept();
            }
            if (s.isReadable()) {
              List<Response> responses = reciveResponse(s);
              if (!responses.isEmpty()) {
                System.out.println(responses.size() + " reponse reçus de la part de "
                    + System.identityHashCode(s));
                responses.forEach(r -> {
                  manager.writePixels(r);
                });
                if (manager.acquit(responses.get(0).getIdRequest()) >= 0)
                  httpServer.selectorHttp.wakeup();
                SocketChannel sc = (SocketChannel) s.channel();
                if (sc.isOpen())
                  freeClient.add(sc);
              }
            }
          }
        });
      } catch (IOException e) {
        e.printStackTrace();
      }
      freeClient.removeIf(s -> !s.isOpen());
      int nbClient = freeClient.size();
      if (nbClient > 0)
        manager.forward(nbClient);
      SocketChannel sc = null;
      while (!freeClient.isEmpty() && manager.hasNext()) {
        Request tmp = manager.getNext();
        sc = freeClient.remove(0);
        if (sc.isConnected())
          System.out.println("Envoie de requete " + tmp.getId() + " à " + sc);
        sendRequest(sc, tmp);
      }

    }

  }

  public static void main(String[] args) throws InterruptedException {

    Server server = new Server(12345);
    MyHttpServer sh = new MyHttpServer(5000, server);
    server.httpServer = sh;
    Thread th1 = new Thread(server);
    Thread th2 = new Thread(sh);
    th2.start();
    th1.start();

  }
}