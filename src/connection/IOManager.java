package connection;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.net.Socket;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

import javax.imageio.ImageIO;

import Serialize.Mandelbrot;
import Serialize.Pixel;
import Serialize.Request;
import Serialize.Response;

public class IOManager {

  private static final IOManager REQUEST_MANAGER = new IOManager();

  LinkedBlockingQueue<Request> queue;
  Map<Integer, List<Request>> readys;
  Map<Integer, Integer> notComputed;
  Map<Integer, BufferedImage> images;
  Map<Integer, Socket> users;
  Map<Integer, Request> historic;

  private IOManager() {
    this.images = new HashMap<>();
    this.users = new HashMap<>();
    this.queue = new LinkedBlockingQueue<>();
    this.readys = new HashMap<>();
    this.notComputed = new HashMap<>();
    this.historic = new HashMap<>();

  }

  public static IOManager getInstance() {
    return REQUEST_MANAGER;
  }

  public synchronized boolean hasNext() {
    return !this.readys.keySet().isEmpty();
  }

  public synchronized void forward(int ComputerNumber) {
    if (queue.size() > 0)
      try {
        Request r = queue.take();

        int dn = (queue.size() == 0) ? 1 : queue.size();

        images.put(r.getId(), new BufferedImage(r.getWidth(), r.getLength(), BufferedImage.TYPE_3BYTE_BGR));
        List<Request> lr = Mandelbrot.subRequest(r, ComputerNumber / dn);
        this.readys.put(r.getId(), lr);
        this.notComputed.put(r.getId(), lr.size());
        this.historic.put(r.getId(), r);
      } catch (InterruptedException e) {
        e.printStackTrace();
      }
  }

  public synchronized void recive(Request r) {
    try {
      this.queue.offer(r, 1, TimeUnit.SECONDS);
      System.out.println(queue.size() + " requetes en attente de traitement ");
    } catch (InterruptedException e) {
      e.printStackTrace();
    }
  }

  public synchronized Request getNext() {

    List<Integer> keys = this.readys.keySet().stream().collect(Collectors.toList());
    Integer key = keys.get(0);
    List<Request> ls = readys.get(key);
    Request r = null;
    List<Request> cs = new ArrayList<>();
    if (ls.size() == 1) {
      r = ls.get(0);
      readys.remove(key);
    } else {
      r = ls.get(ls.size() - 1);
      for (int i = 0; i < ls.size() - 1; i++) {
        cs.add(ls.get(i));
      }
    }
    if (cs.isEmpty())
      readys.remove(key);
    else
      readys.replace(key, cs);
    return r;
  }

  public synchronized int acquit(int key) {
    Integer n = notComputed.get(key);
    if (n == null)
      return -1;
    n--;
    this.notComputed.replace(key, n);
    return n;
  }

  public synchronized String createFileIfReady(Integer id) {
    String img = "";
    Integer computed = notComputed.getOrDefault(id, -1);
    if (computed == 0) {
      try {
        File file = new File(id + ".png");
        file.setWritable(true, true);
        file.setExecutable(true, true);
        file.setReadable(true, true);
        ImageIO.write(images.get(id), "png", file);
        img += id;
        notComputed.remove(id);
      } catch (IOException e) {
        e.printStackTrace();
      }
    }
    return img;
  }

  public synchronized void createFiles() {

    notComputed.forEach((k, v) -> {
      if (v == 0) {
        try {
          ImageIO.write(images.get(k), "png", new File(k + ".png"));
        } catch (Exception e) {
          e.printStackTrace();
        }
      }
    });
    for (Integer i : notComputed.keySet()) {
      if (notComputed.get(i) == 0)
        notComputed.remove(i);
    }

  }

  public synchronized void writePixels(Response r) {
    int key = r.getIdRequest();
    BufferedImage image = images.get(key);
    for (Pixel p : r.getValues())
      image.setRGB(p.getX(), image.getHeight() - 1 - p.getY(), p.getColor());

  }

  public void registerUser(Socket s) {
    users.put(System.identityHashCode(s), s);
  }

}
