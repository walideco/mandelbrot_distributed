package Serialize;

import java.util.concurrent.Callable;

public class Task implements Callable<Response> {

  private Request request;

  public Task() {
  }

  public Task(Request request) {
    this.request = request;
  }

  @Override
  public Response call() throws Exception {
    return new Response(request.getId(), Mandelbrot.compute(request, 0));
  }

}
