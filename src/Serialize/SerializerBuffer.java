package Serialize;

import java.nio.ByteBuffer;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class SerializerBuffer {

  public static Map<Integer, Integer> objMap = new HashMap<>();
  public static ArrayList<MySerialisable> objList = new ArrayList<>();

  public ByteBuffer byteBuffer;

  public SerializerBuffer(int size) {
    this.byteBuffer = ByteBuffer.allocate(size);

  }

  public void writeInt(int i) {
    this.byteBuffer.putInt(i);
  }

  public void writeDouble(double i) {
    this.byteBuffer.putDouble(i);
  }

  public int readInt() {
    return this.byteBuffer.getInt();
  }

  public double readDouble() {
    return this.byteBuffer.getDouble();
  }

  public void writeString(String string) {
    if (string == null)
      this.byteBuffer.putInt(1);
    else {
      this.byteBuffer.putInt(0);
      Charset charset = Charset.forName("UTF-8");
      ByteBuffer bytes = charset.encode(string);
      this.byteBuffer.putInt(bytes.remaining());
      this.byteBuffer.put(bytes);
    }
  }

  public String readString() {
    if (byteBuffer.getInt() == 1)
      return null;
    Charset charset = Charset.forName("UTF-8");
    int n = byteBuffer.getInt();
    int limit = byteBuffer.limit();
    byteBuffer.limit(byteBuffer.position() + n);
    String result = charset.decode(byteBuffer).toString();
    byteBuffer.limit(limit);
    return result;

  }

  public void writeMySerializable(MySerialisable ms) {
    if (ms == null)
      byteBuffer.putInt(1);
    else {
      // Integer identity = System.identityHashCode(ms);
      // Integer index = objMap.getOrDefault(identity, null);
      // if (index == null) {
      // objList.add(ms);
      // objMap.put(identity, objList.indexOf(ms));
      byteBuffer.putInt(0);
      ms.writeToBuff(this);
      // } else {
      // System.out.println(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>"+identity);
      // byteBuffer.putInt(2);
      // byteBuffer.putInt(identity);
      // }

    }
  }

  @SuppressWarnings("unchecked")
  public <T extends MySerialisable> T readMySerializable(Creator<T> c) {
    Integer type = byteBuffer.getInt();
    if (type == 1)
      return null;
    // else if (type == 2) {
    // return (T) objList.get(objMap.get(byteBuffer.getInt()));
    // }
    T t = c.init();
    t.readFromBuff(this);
    return t;
  }

  public void extendSize() {
    ByteBuffer tmp = ByteBuffer.allocate(byteBuffer.capacity() + byteBuffer.capacity() / 2);
    byteBuffer.flip();
    // tmp.limit(tmp.capacity());
    while (byteBuffer.hasRemaining()) {
      tmp.put(byteBuffer.get());
    }
    byteBuffer = tmp;
  }
}
