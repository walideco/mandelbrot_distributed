package Serialize;

@FunctionalInterface
public interface Creator<T extends MySerialisable> {
  public T init();
}
